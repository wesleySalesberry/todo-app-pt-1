import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import './App.css';

library.add(faTrash);

class App extends Component {
	state = {
		todos: [],
		currentTodo: {
			note: '',
			id: ''
		}
	};

	handleInput = (evt) => {
		this.setState({
			currentTodo: {
				id: Date.now(),
				note: evt.target.value,
				isCompleted: false
			}
		});
	};

	handleSubmit = (evt) => {
		evt.preventDefault();
		const newTodo = this.state.currentTodo;

		if (newTodo.note !== '') {
			const todos = [ ...this.state.todos, newTodo ];
			this.setState({
				todos: todos,
				currentTodo: {
					note: '',
					id: '',
					isCompleted: false
				}
			});
		}
	};

	clearCompleted = () => {
		console.log({});

		this.setState({
			todos: this.state.todos.filter((item) => item.isCompleted === false)
		});
	};

	deleteItem = (todosId) => {
		this.setState({
			todos: this.state.todos.filter((item) => item.id !== todosId)
		});
	};

	completedItem = (todosId) => {
		this.setState({
			todos: this.state.todos.map(
				(item) => (item.id === todosId ? { ...item, isCompleted: !item.isCompleted } : item)
			)
		});
	};

	render() {
		return (
			<section className="todoapp">
				<header className="header">
					<h1>todos</h1>
					<form onSubmit={this.handleSubmit}>
						<input
							onChange={this.handleInput}
							checked={this.props.isCompleted}
							id="todo"
							className="new-todo"
							placeholder="What needs to be done?"
							autoFocus
						/>
					</form>
				</header>
				<TodoList completedItem={this.completedItem} deleteItem={this.deleteItem} todos={this.state.todos} />

				<footer className="footer">
					<span className="todo-count">
						<strong>{this.state.todos.length}</strong> {this.state.todos.length <= 1 ? 'item' : 'items'}{' '}
						left
					</span>
					<button
						onClick={() => {
							this.clearCompleted();
						}}
						className="clear-completed"
					>
						Clear completed
					</button>
				</footer>
			</section>
		);
	}
}

class TodoItem extends Component {
	render() {
		return (
			<li className={this.props.isCompleted ? 'completed' : ''}>
				<div className="view">
					<input
						className="toggle"
						type="checkbox"
						value={this.props.isCompleted}
						onChange={this.props.completedItem}
					/>

					<label>{this.props.title}</label>
					{/* <button className="destroy" /> */}
					<span className="destroy">
						<FontAwesomeIcon className="faicon" icon="trash" onClick={this.props.deleteItem} />
					</span>
				</div>
			</li>
		);
	}
}

class TodoList extends Component {
	render() {
		return (
			<section className="main">
				<ul className="todo-list">
					{this.props.todos.map((todo) => (
						<TodoItem
							key={todo.id}
							isCompleted={todo.isCompleted}
							title={todo.note}
							deleteItem={(evt) => this.props.deleteItem(todo.id)}
							completedItem={(evt) => this.props.completedItem(todo.id)}
						/>
					))}
				</ul>
			</section>
		);
	}
}

export default App;
